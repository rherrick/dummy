package org.nrg.xnat.containers.dummy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@Slf4j
public class DummyApplication implements ApplicationRunner {
    public static void main(final String[] arguments) {
        SpringApplication.run(DummyApplication.class, arguments);
    }

    @Override
    public void run(final ApplicationArguments arguments) {
        if (!arguments.containsOption("instance")) {
            _instanceName = "<not passed>";
        } else {
            final List<String> values = arguments.getOptionValues("instance");
            if (values.isEmpty()) {
                _instanceName = "<passed but no value>";
            } else {
                if (values.size() > 1) {
                    log.warn("Passed more than one value for the \"instance\" option: {}", String.join(", ", values));
                }
                _instanceName = values.get(0);
            }
        }
    }

    @GetMapping()
    public Map<String, String> getUuidAndTimestamp() {
        return Map.of(KEY_NAME, _instanceName, KEY_UUID, INSTANCE_UUID, KEY_TIMESTAMP, FORMATTER.format(LocalDateTime.now()));
    }

    private static final String KEY_NAME      = "name";
    private static final String KEY_UUID      = "uuid";
    private static final String KEY_TIMESTAMP = "timestamp";

    private static final String            INSTANCE_UUID = UUID.randomUUID().toString();
    private static final DateTimeFormatter FORMATTER     = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);

    private String _instanceName = "<uninitialized>";
}
